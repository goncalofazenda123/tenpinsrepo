'use strict';


module.exports = function (Scores) {
    
    
    const currentDate = new Date()




    
    // GET NUMBER OF DAYS OF A SPECIFIC MONTH
    function daysInMonth (year, month) {
        return new Date(year, month, 0).getDate()
    }

    
    


    
    // Scores.getScoreTeste = function (user) {
    //     return Scores.find({ where: { user: user } })
    // }







    // GET THE 10 BEST SCORES OF ALL TIME
    Scores.getBestScoresEver = function (){

        return Scores.find( { order: 'score ASC', limit: 10 } )
    }
        




    
        
    // GET THE 10 BEST SCORES OF THE MONTH  
    Scores.getBestScoresMonth = function (){
        
        let year = currentDate.getFullYear()
        let month = currentDate.getMonth()
        let day = currentDate.getDate()

        if ( month == 0 ){
            year--
            month = 11
        }else{
            month-- 
        }
        
        // January = 00 it so needs to increment 1 in order to display correct info (to Query).
        month = month + 1

        let bestMonthScores = Scores.find({ 
            where: { data: { lt: currentDate, gt: year+"-"+month+"-"+day }  },
            order: 'score ASC', limit: 10 
        })

        return bestMonthScores 
    }




    




    // GET THE BEST 10 SCORES OF THE WEEK
    Scores.getBestScoresWeek = function (){
        
        
        let year = currentDate.getFullYear()
        let month = currentDate.getMonth()
        let day = currentDate.getDate()

        // let year = 2022
        // let month = 0
        // let day = 7
        // console.log("Day ",day," Month ",month," Year ",year)

        let diff = 0

        // GETTING THE DATE ONE WEEK AGO
        if ( month == 0 && day <= 7 ){
            year--
            month = 11
            diff = 7-day
            day = daysInMonth(year,month) - diff
        }else if ( day <= 7 ){
            month--
            diff = 7-day
            day = daysInMonth(year,month) - diff
        }else{
            day-=7
        }

        
        //console.log("Day ",day," Month ",month," Year ",year)


        // January = 00 it so needs to increment 1 in order to display correct info (to Query).
        month = month + 1        
        
    
        let bestWeekScores = Scores.find({ 
            where: { data: { lt: currentDate, gt: year+"-"+month+"-"+day }  },
            order: 'score ASC', limit: 10 
        })

        return bestWeekScores
    }
    




    // CREATES ANOTHER INSTANCE IN THE DATABASE
    Scores.setNewScore = function (score){
        console.log("RECEBIIIII ISTOOOO !!! " , score[0])
        
        // let data = {
        //     "user": "Teste",
        //     "score": 202,
        //     "data": "2035-04-01T05:14:29.830Z"
        //   }

        return Scores.create(score)

    }








    // --------------------------- REMOTE METHODs ------------------------
    
    Scores.remoteMethod(
        'getBestScoresWeek', {
            returns: { arg: 'score', type: 'object' },
            http: {
                path: '/getBestScoresWeek',
                verb: 'get'
            }
        }
    )
    Scores.remoteMethod(
        'getBestScoresMonth', {
            returns: { arg: 'score', type: 'object' },
            http: {
                path: '/getBestScoresMonth',
                verb: 'get'
            }
        }
    )
    Scores.remoteMethod(
        'getBestScoresEver', {
            returns: { arg: 'score', type: 'object' },
            http: {
                path: '/getBestScoresEver',
                verb: 'get'
            }
        }
    )
    Scores.remoteMethod(
        'setNewScore', {
            accepts: { arg: 'scossssssre', type: 'object' },
            returns: { arg: 'score', type: 'object' },
            
            http: {
                path: '/setNewScore',
                verb: 'post'
            }
        }                          
    )

};





