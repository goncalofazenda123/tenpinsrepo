import React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import Tabs from './routes/tabs';
import store from './redux/store/store'
import { Provider } from 'react-redux';



export default function App(){
  return(
    <Provider store={store}>
      <NavigationContainer>
        <Tabs/>
      </NavigationContainer>
    </Provider>
  )
}
