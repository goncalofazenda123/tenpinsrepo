import React from "react";
import { StyleSheet ,View ,Text, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { updateDices, updatelockedNumbers, updateIsPlaying, updatePersonalScore, clearTryingNumbers, updateCollorButtonFlag, updateLetsRollButtonText } from '../redux/actions/actions'





//export default function Menu({ navigation }){

class Menu extends React.Component{


    startNewGame = () => {
        this.props.updateDices(6,6)
        this.props.updatelockedNumbers([])
        this.props.updateIsPlaying(false)
        this.props.updatePersonalScore(55)
        this.props.clearTryingNumbers()
        this.props.updateLetsRollButtonText("Let's Roll!")
        //this.props.updateCollorButtonFlag(false)

        this.props.navigation.navigate({ name: "Game"})
    }

    
    render(){
        return(
            // <Tab.Navigator screenOptions={{tabBarIconStyle: {display: "none"}, tabBarLabelStyle:{fontSize: 21}}} >
            //     <Tab.Screen name="Game" component={GameScreen} />
            // </Tab.Navigator>


            <View style={styles.menuMainView}>
                <TouchableOpacity style={styles.toutchableStartGame} onPress={()=> this.startNewGame() }>
                    <Text style={styles.newGameButton}>Start New Game</Text>
                </TouchableOpacity>

            </View>
        )
    }
}




// Actions Trigger
function mapDispatchToProps(dispatch) {
    return{
        updateDices: (dice1,dice2) => dispatch(updateDices(dice1,dice2)),
        updatelockedNumbers: (array) => dispatch(updatelockedNumbers(array)),
        updateIsPlaying: (value) => dispatch(updateIsPlaying(value)),
        updatePersonalScore: (value) => dispatch(updatePersonalScore(value)),
        clearTryingNumbers: () => dispatch(clearTryingNumbers()),
        updateCollorButtonFlag: (value) => dispatch(updateCollorButtonFlag(value)),
        updateLetsRollButtonText: (value) => dispatch(updateLetsRollButtonText(value)),
    }
}

export default connect(0,mapDispatchToProps)(Menu)







const styles = StyleSheet.create({

    newGameButton:{
        fontSize: 25,  
        backgroundColor:'#3dba27',
        padding:40,
        textAlign:'center',
        fontWeight:'bold'
    },

    toutchableStartGame:{
        backgroundColor:'red',
        justifyContent:'center',
    },

    menuMainView:{
        flex:1,
        justifyContent:'center',
    }
})