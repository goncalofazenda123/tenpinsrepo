import React from "react";
import { StyleSheet, View, Text, TextInput, TouchableOpacity, Modal , Keyboard} from "react-native";
import axios from "axios";




class SubmitScore extends React.Component {


    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            username : ''
        }
    }

    
    
    
    handleModal = () =>{
        this.setState({
            modal : this.state.modal ? false : true
        })
    }




    
    handleUserInput = (username) =>{
        this.setState({
            username : username
        })
    }






    
    setNewScore = async () => {
        let todaysDate = new Date()
        let username = this.state.username
        let score = this.props.score

        let data = {"user": username,"score": score,"data": todaysDate}
        

        if( username.length <= 9 && username != '' ){
            await axios.post('http://localhost:3000/api/scores',data)
                .catch(error => {
                    console.log("ERROR: ",error)
                }
            );
            this.handleModal()
        } else if( username == '' ){
            alert('Please, introduce a username.')
        } else {
            alert('Username must have between 1 and 9 characters')
        }
    }




    render(){

        return(
            <View style={ styles.mainView }>

                <TouchableOpacity onPress={()=> this.handleModal()} >
                    <Text style={ styles.doneButtonText }>Done</Text>
                </TouchableOpacity>


                <Modal  visible={ this.state.modal ? true : false } transparent={true} animationType={'fade'} >
                    <View style={ styles.outerView } >
                        <TouchableOpacity style={{flex:1}} onPress={() => this.handleModal()} />
                    </View>


                    <View style={ styles.modalMainView }>
                        <Text style={ styles.messageText }>Tell us your name Pro !! </Text>
                        <TextInput style={ styles.usernameInput } placeholder='example: parasita' onSubmitEditing={Keyboard.dismiss} onChangeText={(username)=>this.handleUserInput(username)}></TextInput>

                        <View style={{ justifyContent:'space-between', flexDirection: 'row', marginTop:20 }}>
                            <TouchableOpacity style={ styles.backButton } onPress={() => this.handleModal()} >
                                <Text style={{ fontSize: 20 }}>Back</Text>
                            </TouchableOpacity>

                            <TouchableOpacity style={ styles.submitButton} onPress={() => this.setNewScore()} >
                                <Text style={{ fontSize: 20 }}>Submit</Text>
                            </TouchableOpacity>
                        </View>
                    </View>


                    <View style={ styles.outerView } >
                        <TouchableOpacity style={{flex:1}} onPress={() => this.handleModal()} />
                    </View>
                </Modal>

            </View>
        )
    }
}

export default SubmitScore




const styles = StyleSheet.create({

    mainView:{
        width : '100%',
        marginLeft : '40%',
        backgroundColor : 'orange',
        borderRadius : 30
    },

    outerView:{
        flex : 1,
        backgroundColor : 'grey',
        opacity: 0.5
    },

    modalMainView:{
        paddingHorizontal:15,
        paddingVertical:50,
        backgroundColor : 'white',
        flex : 0.5,
        borderColor : 'orange',
        borderWidth : 2,
        borderStyle : 'dashed'
    },

    doneButtonText:{
        fontSize : 25,
        fontWeight : '500',
        textAlign : 'left',
        paddingLeft : 35,
        paddingTop : 10,
        paddingBottom :10
    },

    messageText:{
        marginTop: 10,
        paddingLeft:15,
        fontSize:26,
        fontWeight:'500',
        marginBottom: 20
    },

    usernameInput:{
        fontSize:20,
        fontWeight:'300',
        backgroundColor:'orange',
        paddingVertical:10,
        paddingHorizontal:20,
        borderRadius:30
    },

    backButton:{
        paddingVertical:10,
        paddingHorizontal:20,
        backgroundColor:'#ededed',
        borderRadius:30
    },

    submitButton:{
        paddingVertical:10,
        paddingHorizontal:20,
        backgroundColor:'#3dba27',
        borderRadius:30
    }





})


