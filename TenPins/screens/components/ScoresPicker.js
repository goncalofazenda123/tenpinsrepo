import React from "react";
import { Picker } from '@react-native-picker/picker'

import { connect } from "react-redux";
import { updateScoresToShow , updateScoreboardData } from '../../redux/actions/actions';
import { getScoresList } from '../../functions/ApiGetsSets'



class ScoresPicker extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            pickerLable: [ 'Best Scores Ever', 'Scores of the Month', 'Scores of the Week' ],
            scoresToShow: 'Best Scores Ever'
        }
    }


    componentDidMount(){
        this.pickerHandler(this.state.scoresToShow)
    }


    pickerHandler = async ( value ) =>{
        let path = ''
        let data = []
        this.setState({ scoresToShow: value })
        
        if ( value == 'Best Scores Ever'){
            path = 'getBestScoresEver'
        }else if ( value == 'Scores of the Month'){
            path = 'getBestScoresMonth'
        }else if ( value == 'Scores of the Week'){
            path = 'getBestScoresWeek'
        }

        data = await getScoresList(path)

        this.props.updateScoreboardData(data)
    }

 

    render(){
        return(
                <Picker 
                    style={{ flex: 1, marginBottom:100}}
                    selectedValue={this.state.scoresToShow} 
                    onValueChange={ ( value ) => this.pickerHandler(value) } 
                >
                    {
                        this.state.pickerLable.map( item => {
                            return <Picker.Item label={ item } value={ item } key={ item }/>
                        })
                    }
                </Picker>
        )
    }
}





function mapStateToProps(state) {
    return{
        scoresToShow: state.scoresToShow,
        scoreboardData: state.scoreboardData
    }
}


// Actions Trigger
function mapDispatchToProps(dispatch) {
    return{
        updateScoreboardData: (data) => dispatch( updateScoreboardData( data ))
    }
}



export default connect( mapStateToProps,mapDispatchToProps )( ScoresPicker )
