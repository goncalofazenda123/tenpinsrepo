import React from "react";
import { StyleSheet, View, Button, Text, Alert, TouchableOpacity, ActivityIndicator, Image } from "react-native";
import axios from 'axios'
import { globalStyles } from "../../styles/global";
import { connect } from "react-redux";
import { updateDices, updateScore } from '../../redux/actions/actions'


class Dices extends React.Component {


    // Dice Generator
    randomDiceGenerator = () =>{
        let randomDice = Math.floor(Math.random()*6)+1
        return (randomDice)
    } 


    rollDices = () =>{
        let dice1 = this.randomDiceGenerator()
        let dice2 = this.randomDiceGenerator()

        this.props.updateDices(dice1,dice2)
    }

    render() {
        return (
                <View style={styles.container}>    
                    <Image style={styles.dice} source={this.props.diceimg[this.props.dice1]}/>
                    <Text style={styles.textDiceContainer}>+</Text>
                    <Image style={styles.dice} source={this.props.diceimg[this.props.dice2]}/>
                    <Text style={styles.textDiceContainer}>= {this.props.dicesResult}</Text>
                </View>
        );
    }
}

// Map store to props
function mapStateToProps(state) {
    return{
        dice1: state.dice1,
        dice2: state.dice2,
        dicesResult: state.dicesResult,
        diceimg:{
            1:state.diceimg[1],
            2:state.diceimg[2],
            3:state.diceimg[3],
            4:state.diceimg[4],
            5:state.diceimg[5],
            6:state.diceimg[6],
        }
    }
}

// Actions Trigger
function mapDispatchToProps(dispatch) {
    return{
        updateDices: (dice1,dice2) => dispatch(updateDices(dice1,dice2)),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(Dices)



const styles = StyleSheet.create({

    container:{
        justifyContent:'space-between',
        alignItems:'center',
        flexDirection:'row',
        marginHorizontal:20,
        marginVertical:60,
    },

    dice:{
        height:100,
        width:100,
    },
    
    textDiceContainer:{
        fontSize: 50,
        fontWeight:'500',
    }
})

