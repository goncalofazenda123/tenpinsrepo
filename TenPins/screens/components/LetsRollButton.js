import React from "react";
import { StyleSheet, View, Text, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import { updateDices, updatelockedNumbers, updateIsPlaying, updatePersonalScore, clearTryingNumbers , updateCollorButtonFlag, updateLetsRollButtonText} from '../../redux/actions/actions'






class LetsRollButton extends React.Component {
    
    constructor(props){
        super(props)
        this.state={
            buttonText:"Let's Roll!",
            tryingNumbersTotal: 0,
            notUsedNumbers : []
        }
    }



    
    // Dice Generator
    randomDiceGenerator = () => {
        let randomDice = Math.floor(Math.random()*6)+1
        return (randomDice)
    } 






    rollDices = async () => {
        let dice1 = this.randomDiceGenerator()
        let dice2 = this.randomDiceGenerator()
        // let dice2img = '../img/dices/'+dice2+'.pgn'
        await this.props.updateDices(dice1,dice2)
        this.updateButtonStatus()

        this.props.updateCollorButtonFlag(true)
    }

    




    getTryingNumbersTotal = () => {
        const element = this.props.tryingNumbers
        let res = 0
        for ( let i=0; i < element.length ; i++ ){
            res = res + element[i]
        }
        this.setState({ tryingNumbersTotal: res })
    }






    getNotUsedNumbers = () => {

        let element = this.props.lockedNumbers
        let notUsedNumbers = []
        
        
        for( let i=0; i < 10; i++){
            
            if( element.includes(i+1) ){
            }else{
                notUsedNumbers.push(i+1) 

            }
        }

        this.setState({ notUsedNumbers : notUsedNumbers })
        console.log(notUsedNumbers)
    }







    updateButtonStatus = async () => {
        if( this.props.letsRollButtonText == "Let's Roll!" ){

            // Player Rolls Dices
            this.props.updateLetsRollButtonText("Lock Answer")
            this.props.updateIsPlaying(true)
        }else{

            // Player Locks Answer

            await this.getNotUsedNumbers()
            await this.getTryingNumbersTotal()
            let tryingNumbersTotal = this.state.tryingNumbersTotal
            let dicesTotal = this.props.dicesTotal
            let numbersToLock = []
            let lowestRemainingNumber = Math.min(...this.state.notUsedNumbers)


            console.log('not used numbers ', this.state.notUsedNumbers)
            console.log('lowest nuber ', lowestRemainingNumber)

            if(dicesTotal > lowestRemainingNumber ){
                if( tryingNumbersTotal == dicesTotal){
                    numbersToLock = [...this.props.lockedNumbers , ...this.props.tryingNumbers]
                    this.props.updateIsPlaying(false)
                    this.props.updateLetsRollButtonText("Let's Roll!")
                    this.props.clearTryingNumbers()
                }else{
                    alert(' A soma dos numeros não é igual à soma de dados.')
                    numbersToLock = [...this.props.lockedNumbers]
                }
            }
            await this.props.updatelockedNumbers(numbersToLock)
        }
        
        this.personalScore()
    }








    personalScore = () => {
        const numbers = this.props.lockedNumbers
        var res = 0
        
        for(let i=0 ; i < numbers.length ; i++){
            res= res + numbers[i]
        }

        this.props.updatePersonalScore(55-res)
    }    






    render(){
        return(
            <View style={ styles.rollDiceConteiner }>      
                <TouchableOpacity 
                    onPress={ this.props.letsRollButtonText == "Let's Roll!" ? this.rollDices : this.updateButtonStatus} 
                    style={ this.props.letsRollButtonText == "Let's Roll!" ? styles.letsRoll : styles.lockAnswer }
                >
                    <Text style={ styles.rollDiceButton }> {this.props.letsRollButtonText} </Text>
                </TouchableOpacity>


            </View>
        )
    }

}



function mapStateToProps(state) {
    return{
        score : state.score,
        dicesTotal : state.dicesResult,
        tryingNumbers : state.tryingNumbers,
        lockedNumbers : state.lockedNumbers,
        letsRollButtonText : state.letsRollButtonText,
    }
}

// Actions Trigger
function mapDispatchToProps(dispatch) {
    return{
        updateDices: (dice1,dice2) => dispatch(updateDices(dice1,dice2)),
        updatelockedNumbers: (array) => dispatch(updatelockedNumbers(array)),
        updateIsPlaying: (value) => dispatch(updateIsPlaying(value)),
        updatePersonalScore: (value) => dispatch(updatePersonalScore(value)),
        clearTryingNumbers: () => dispatch(clearTryingNumbers()),
        updateCollorButtonFlag: (value) => dispatch(updateCollorButtonFlag(value)),
        updateLetsRollButtonText: (value) => dispatch(updateLetsRollButtonText(value)),
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(LetsRollButton)









const styles = StyleSheet.create({

    rollDiceConteiner: {
        alignItems: 'center',
    },

    rollDiceButton: {
        fontSize: 30,
        fontWeight: '600',
        textAlign: 'center',
        paddingVertical: 17,
    },
        
    letsRoll: {
        width: '100%',
        backgroundColor: '#3dba27'
    },

    lockAnswer: {
        width: '100%',
        backgroundColor: 'red'
    },

    rollDiceAreaRed:{
        width: '100%',
        backgroundColor: 'grey'
    },
})