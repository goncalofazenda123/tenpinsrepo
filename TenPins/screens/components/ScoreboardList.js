import React from "react";
import { FlatList, Text , StyleSheet, View} from "react-native";

import { connect } from "react-redux";



class ScoreboardList extends React.Component{



    render(){
        return(

            <View style={styles.mainView}>
                <View style={{ flexDirection:'row',backgroundColor: 'orange' ,borderTopWidth:1, borderBottomWidth:1 , justifyContent:'space-between' }}>
                    <Text style={styles.textHeaderUser}> User </Text>
                    <Text style={styles.textHeaderScore}> Score </Text>
                </View>

                <FlatList 
                    data={this.props.scoreboardData}
                    renderItem={ ( { item } ) => { 
                        return(  
                            <View style={{ flexDirection:'row', borderBottomWidth:0.2 , justifyContent:'space-around' }}>
                                <Text style={styles.textUser}>{ item.user }</Text>
                                <Text style={styles.textScore}>{ item.score }</Text>
                            </View>
                        )
                    }
                }>
                </FlatList>
            </View>
        )
    }
}





function mapStateToProps(state) {
    return{
        scoreboardData: state.scoreboardData
    }
}


export default connect( mapStateToProps, null )( ScoreboardList )






const styles = StyleSheet.create({

    mainView:{
        flex:4
    },

    textHeaderUser:{
        paddingVertical:8,
        paddingLeft:70,
        fontSize: 30,
        fontWeight: '600'
    },
    textHeaderScore:{
        paddingVertical:8,
        paddingRight:40,
        fontSize: 30,
        fontWeight: '600'
    },

    textUser:{
        paddingVertical:8,
        fontSize: 25,
        fontWeight: '400'
    },

    textScore:{
        paddingVertical:8,
        fontSize: 25,
        fontWeight: '500'
    }

})
