import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import { updateTryingNumbers } from '../../redux/actions/actions';
import { connect } from "react-redux";




class NumberButton extends React.Component {


    constructor(props){
        super(props)
        this.state={
            coloredButtonFlag : true,
        }
    }



    buttonActions = async () => {

        const element = this.props.tryingNumbers.includes(this.props.number)

            if(!element){
                this.setState({ coloredButtonFlag : false })
            }else{
                this.setState({ coloredButtonFlag : true })
            }


        await this.props.updateTryingNumbers(this.props.number)
    }
    


    isDisabled = () => {
        const number = this.props.number
        const isPlaying = this.props.isPlaying
        const element = this.props.lockedNumbers.includes(number)

        if(isPlaying){
            if(element) return true
        }else{
            return true
        }
    }



    render(){
        
        const dis = this.isDisabled()
        
        return(   

            <TouchableOpacity 
            style={ this.state.coloredButtonFlag ? styles.button  : styles.buttonPressed } 
            onPress={ ()=> this.buttonActions() }                
            disabled={dis}
            >
                <Text style={ styles.buttonText }>{ this.props.number }</Text>
            </TouchableOpacity>
        )
    }
}





function mapStateToProps(state) {
    return{
        score : state.score,
        dicesResult : state.dicesResult,
        roundTotal : state.roundTotal,
        tryingNumbers : state.tryingNumbers,
        lockedNumbers : state.lockedNumbers,
        isPlaying : state.isPlaying,
        colorButtonFlag : state.colorButtonFlag
    }
}


function mapDispatchToProps(dispatch) {
    return{
        updateTryingNumbers: (number) => dispatch( updateTryingNumbers( number ))
    }
}


export default connect( mapStateToProps,mapDispatchToProps )( NumberButton )








const styles = StyleSheet.create({

        button:{
            flex:1,
            alignItems:'center',
            justifyContent:'center',
            height:100,
            borderRadius:30,
            margin:5,
            backgroundColor:'#03a5fc',
        },
        buttonPressed:{
            flex:1,
            alignItems:'center',
            justifyContent:'center',
            height:100,
            borderRadius:30,
            margin:5,
            backgroundColor:'grey'
        },
        buttonText:{
            textAlign:'center',
            fontSize:25
        }

})

