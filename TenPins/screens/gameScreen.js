import React from "react";
import { StyleSheet, View, Text, ActivityIndicator } from "react-native";
import { connect } from "react-redux";

// COMPONENTS
import NumberButton from './components/NumberButton';
import Dices from './components/Dices';
import LetsRollButton from "./components/LetsRollButton";
import SubmitScore from "./components/SubmitScore";









class GameScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            
        }
    }



    render() {
        return (
            <View style={styles.mainView}>    

                <View style={styles.diceConteiner}>
                    <Dices/>
                </View>

                        
                <View style={{flexDirection:'row'}}>
                    <Text style={styles.textTotalScore}>Total Score : </Text>
                    <Text style={styles.textTotalScoreValue}>{this.props.personalScore}</Text>
                    <View>
                        <SubmitScore score={this.props.personalScore} />
                    </View>
                </View>
       

                <View style={styles.playerAreaConteiner}>
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-end' }}>
                        <NumberButton number={1} />
                        <NumberButton number={2} />
                        <NumberButton number={3} />
                        <NumberButton number={4} />
                        <NumberButton number={5} />
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row'}}>
                        <NumberButton number={6} />
                        <NumberButton number={7} />
                        <NumberButton number={8} />
                        <NumberButton number={9} />
                        <NumberButton number={10} />
                    </View>
                </View>

                <LetsRollButton/> 
                          
            </View>
        );
    }
}

// Map store to props
function mapStateToProps(state) {
    return{
        personalScore: state.personalScore,
        dice1: state.dice1,
        dice2: state.dice2,
        dicesResult: state.dicesResult,
        diceimg:{
            1:state.diceimg[1],
            2:state.diceimg[2],
            3:state.diceimg[3],
            4:state.diceimg[4],
            5:state.diceimg[5],
            6:state.diceimg[6],
        },
        scoresToShow: state.scoresToShow,
        tryingNumbers:state.tryingNumbers
    }
}





export default connect(mapStateToProps)(GameScreen)









const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        justifyContent: 'space-between',
        backgroundColor:'white'
    },

    diceConteiner: {
        marginTop:30,
        backgroundColor:'#ededed',
    },

    playerAreaConteiner: {
        marginBottom:80,
        alignItems: 'center',
    },

    textTotalScore:{
        paddingLeft: 5,
        paddingTop: 10,
        marginBottom: 80,
        textAlign: 'center',
        fontSize: 25,
        fontWeight: '400'
    },
    textTotalScoreValue:{
        paddingLeft:5,
        paddingTop: 8,
        fontSize: 30,
        fontWeight: '600'
    }
    

})