import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import React from "react";
import Menu from '../screens/menu';
import GameScreen from '../screens/gameScreen';
import Scoreboard from '../screens/scoreboard';


const Tab = createBottomTabNavigator();

export default function Tabs() {
  return (
      <Tab.Navigator screenOptions={{tabBarIconStyle: {display: "none"}, tabBarLabelStyle:{fontSize: 21}}} >
        <Tab.Screen name="Menu" component={Menu}/>
        <Tab.Screen name="Game" component={GameScreen} />
        <Tab.Screen name="Scores" component={Scoreboard} />
      </Tab.Navigator>
  );
}


