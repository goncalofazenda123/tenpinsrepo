import axios from "axios";

// Api Fetch
export async function getScoresList( path ){
    let data = null

    await axios.get('http://localhost:3000/api/scores/'+path)
    .then(function (response) {
        data = response.data.score
    })
    .catch(function (error) {
        console.log(error);
    })
    
    return (data)
}




