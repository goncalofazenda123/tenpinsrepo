import { StyleSheet } from "react-native";

export const globalStyles = StyleSheet.create({


    titleText: {
        fontSize: 25,
        color: '#333',
        fontWeight:'400'
    },

    regularText:{
        fontSize: 10,
    },


});