
export const updateDices = (dice1,dice2) => ({
    type: 'Update_Dices',
    payload:{
        dice1:dice1,
        dice2:dice2,
    }
})

export const updateIsPlaying = value => ({
    type: 'Update_isPlaying',
    payload: value,
})

export const updateTryingNumbers = number => ({
    type: 'Update_TryingNumbers',
    payload: number,
})

export const clearTryingNumbers = () => ({
    type: 'Clear_TryingNumbers',
})

export const updatelockedNumbers = array => ({
    type: 'Update_lockedNumbers',
    payload: array,
})

export const updatePersonalScore = value => ({
    type: 'Update_personalScore',
    payload: value,
})

export const updateCollorButtonFlag = value => ({
    type: 'Update_CollorButtonFlag',
    payload: value,
})

export const updateLetsRollButtonText = value => ({
    type: 'Update_LetsRollButtonText',
    payload: value,
})

export const updateScoresToShow = value => ({
    type: 'Update_ScoresToShow',
    payload: value,
})

export const updateScoreboardData = data => ({
    type: 'Update_ScoreboardData',
    payload: data,
})









