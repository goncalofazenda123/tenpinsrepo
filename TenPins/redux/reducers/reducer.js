
const inicialState = {
    personalScore : 55,
    //----- Dice Component -----//
    dice1 : 6,
    dice2 : 6,
    dicesResult : 12,
    diceimg: {
        1:require('../../assets/dices/1.png') ,
        2:require('../../assets/dices/2.png') ,    
        3:require('../../assets/dices/3.png') ,    
        4:require('../../assets/dices/4.png') ,    
        5:require('../../assets/dices/5.png') ,    
        6:require('../../assets/dices/6.png') ,    
    },
    
    //---- NumberButton Component ----//
    
    isPlaying : false,
    colorButtonFlag : false,
    lockedNumbers : [],
    tryingNumbers : [],
    letsRollButtonText : "Let's Roll!",

    //---- ScoresPicker Component ----//

    scoresToShow: 'Best Scores Ever',
    scoreboardData: []
}




const reducer = (state = inicialState, action)=>{

    switch(action.type){
        case 'Update_personalScore':
            return{...state,
                personalScore: action.payload
            }

        case 'Update_Dices':
            return{...state,
                dice1 : action.payload.dice1,
                dice2 : action.payload.dice2,
                dicesResult : action.payload.dice1 + action.payload.dice2 
            }

        case 'Update_isPlaying':
            return{
                ...state,
                isPlaying : action.payload
            }
        
        case 'Update_CollorButtonFlag':
            return{
                ...state,
                colorButtonFlag : action.payload
            }

        case 'Update_TryingNumbers':

            const element = state.tryingNumbers.includes(action.payload)
            const tryingNumbers = state.tryingNumbers

            if(element){
                index = tryingNumbers.indexOf(action.payload)
                tryingNumbers.splice(index,1)
            }else{
                tryingNumbers.push(action.payload)
            }
            return{
                ...state,
                tryingNumbers : tryingNumbers
            }


        case 'Clear_TryingNumbers':
            return{
                ...state,
                tryingNumbers : []
            }

        case 'Update_LetsRollButtonText':
            return{
                ...state,
                letsRollButtonText : action.payload
            }


        case 'Update_lockedNumbers':

            const newLockedNumbers = action.payload

            return{
                ...state,
                lockedNumbers : newLockedNumbers
            }
        
        case 'Update_ScoresToShow':
            return {
                ...state,
                scoresToShow: action.payload
            }
        case 'Update_ScoreboardData':
            return {
                ...state,
                scoreboardData: action.payload
            }
        default : return state
        }
    }
export default reducer

